const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/





//========================================================================================

// Q1. Find all the movies with total earnings more than $500M. 

function allMoviesMoreThan50M(favouritesMovies) {
    favouritesMovies = Object.entries(favouritesMovies);
    // console.log(favouritesMovies);
    const result = favouritesMovies.filter(movie => {
        const { totalEarnings } = movie[1];
        const earning = Number(totalEarnings.replace(/[$M]/g, ""));
        return earning > 500;
    });
    return Object.fromEntries(result);
};

console.log(allMoviesMoreThan50M(favouritesMovies));



//========================================================================================

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

function allMoviesWith3OscarAndMoreThan50M(favouritesMovies) {
    favouritesMovies = Object.entries(favouritesMovies);
    // console.log(favouritesMovies);
    const result = favouritesMovies.filter(movie => {
        const { totalEarnings } = movie[1];
        const { oscarNominations } = movie[1];
        const earning = Number(totalEarnings.replace(/[$M]/g, ""));
        // console.log(earning, oscarNominations);
        return earning > 500 && oscarNominations > 3;
    });
    return Object.fromEntries(result);
};

console.log(allMoviesWith3OscarAndMoreThan50M(favouritesMovies));


//========================================================================================

// Q.3 Find all movies of the actor "Leonardo Dicaprio".

function findLeonardoDicaprioMovie(favouritesMovies) {
    favouritesMovies = Object.entries(favouritesMovies);
    const result = favouritesMovies.filter(movie => {
        const { actors } = movie[1];
        const arr = actors.filter(actor => {
            return actor === 'Leonardo Dicaprio';
        })
        return arr.length >= 1;
    });
    return Object.fromEntries(result);
};

console.log(findLeonardoDicaprioMovie(favouritesMovies));


//========================================================================================


// Q.4 Sort movies (based on IMDB rating)
//         if IMDB ratings are same, compare totalEarning as the secondary metric.


function sortMovieBasedIMDB(favouritesMovies) {
    // favouritesMovies = Object.entries(favouritesMovies);
    const keys = Object.keys(favouritesMovies);

    const result = keys.sort((key1, key2) => {
        const movie1 = favouritesMovies[key1];
        const movie2 = favouritesMovies[key2];

        const imdbRating1 = movie1.imdbRating;
        const imdbRating2 = movie2.imdbRating;
        if (imdbRating1 != imdbRating2) {
            return imdbRating2 - imdbRating1;
        } else {
            const totalEarnings1 = movie1.totalEarnings;
            const totalEarnings2 = movie2.totalEarnings;
            const earning1 = Number(totalEarnings1.replace(/[$M]/g, ""));
            const earning2 = Number(totalEarnings2.replace(/[$M]/g, ""));
            return earning2 - earning1;
        };
    });

    const ans = result.map(key => {
        return [key, favouritesMovies[key]];
    })
    return Object.fromEntries(ans);
    
};

console.log(sortMovieBasedIMDB(favouritesMovies));

//========================================================================================

// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime

function groupMovie(favouritesMovies) {
    const keys = Object.keys(favouritesMovies);
    const newObj = {
        "drama": {},
        "sci-fi": {},
        "adventure": {},
        "thriller": {},
        "crime": {},
    }
    const result = keys.reduce((obj, key) => {
        const movie = favouritesMovies[key];
        const { genre } = movie;
        
        if (genre.includes("drama")) {
            obj["drama"][key] = movie; 
        } else if (genre.includes("sci-fi")) {
            obj["sci-fi"][key] = movie;
        } else if (genre.includes("adventure")) {
            obj["adventure"][key] = movie;
        } else if (genre.includes("thriller")) {
            obj["thriller"][key] = movie;
        } else if (genre.includes("crime")) {
            obj["crime"][key] = movie;
        }
        return obj;
    }, newObj);
    return result;
};

console.log(groupMovie(favouritesMovies));










